import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { ImmortalDB } from 'immortal-db';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  async canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    const credentialTokenDB = await ImmortalDB.get('credentialToken', null);
    if (credentialTokenDB !== null) {
      return true;
    } else {
      await this.router.navigateByUrl('/');
      return false;
    }
  }
}
