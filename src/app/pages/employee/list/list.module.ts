import { NgModule } from '@angular/core';

import { NzTableModule } from 'ng-zorro-antd/table';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { CurrencyPipe, DatePipe, KeyValuePipe, NgForOf, NgIf, TitleCasePipe } from '@angular/common';

import { NzTypographyModule } from 'ng-zorro-antd/typography';
import { NzDropDownModule } from 'ng-zorro-antd/dropdown';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzIconModule } from 'ng-zorro-antd/icon';

import { ListComponent } from './list.component';
import { ListRoutingModule } from './list-routing.module';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { FormsModule } from '@angular/forms';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzDrawerModule } from 'ng-zorro-antd/drawer';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';

import { CamelCaseToTitleCasePipe } from '../../../common/pipe/camel-case-to-title-case.pipe';
import { EmployeeService } from '../../../services/employee.service';

@NgModule({
  imports: [
    NzTableModule,
    NzDividerModule,
    NgForOf,
    NzTypographyModule,
    DatePipe,
    NzDropDownModule,
    NzInputModule,
    NzButtonModule,
    NzIconModule,
    ListRoutingModule,
    NzGridModule,
    FormsModule,
    NzBadgeModule,
    NgIf,
    NzDrawerModule,
    NzDescriptionsModule,
    KeyValuePipe,
    TitleCasePipe,
    CurrencyPipe,
  ],
  providers: [EmployeeService],
  declarations: [ListComponent, CamelCaseToTitleCasePipe],
  exports: [ListComponent],
})
export class ListModule {}
