import { Component, OnInit } from '@angular/core';

import { capitalize, debounce } from 'radash';
import { NzMessageService } from 'ng-zorro-antd/message';

import { EmployeeService } from '../../../services/employee.service';

@Component({
  selector: 'app-employee-list-welcome',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.less'],
})
export class ListComponent implements OnInit {
  constructor(private employeeService: EmployeeService, private message: NzMessageService) {}

  employeeData: Array<EmployeeType> = [];

  employeeLoading: boolean = true;

  employeeSearchValue: string = '';

  employeeInformation: Record<string, string> = {};

  employeeInformationVisible: boolean = false;

  returnZero() {
    return 0;
  }

  createMessage(type: string, message: string): void {
    this.message.create(type, `This is a message of ${message}`);
  }

  setEmployeeInformation(employee: EmployeeType) {
    this.employeeInformation = employee as unknown as Record<string, string>;
  }

  openCloseEmployeeInformationPanel() {
    this.employeeInformationVisible = !this.employeeInformationVisible;
  }

  async fetchEmployee() {
    this.employeeLoading = true;
    this.employeeData = await this.employeeService.findAll();
    this.employeeLoading = false;
  }

  async searchEmployee(searchKey: string) {
    this.employeeLoading = true;
    this.employeeData = await this.employeeService.findOne(searchKey);
    this.employeeLoading = false;
  }

  employeeSearchChange = debounce({ delay: 500 }, async (searchKey: string) => {
    await this.searchEmployee(searchKey);
  });

  employeeInformationClick = debounce({ delay: 200 }, async (employee: EmployeeType) => {
    this.setEmployeeInformation(employee);
    this.openCloseEmployeeInformationPanel();
  });

  async ngOnInit(): Promise<void> {
    await this.fetchEmployee();
  }
}
