import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { NzMessageService } from 'ng-zorro-antd/message';
import { differenceInCalendarDays } from 'date-fns';

import { EmployeeService } from '../../../services/employee.service';
import { EMPLOYEE_GROUP_MAPPER, EMPLOYEE_STATUS_MAPPER } from '../../../common/mapper/employee.mapper';

@Component({
  selector: 'app-employee-form-component',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.less'],
})
export class FormComponent implements OnInit {
  constructor(
    private employeeService: EmployeeService,
    private message: NzMessageService,
    private fb: UntypedFormBuilder,
    private router: Router
  ) {}

  validateForm!: UntypedFormGroup;

  autoTips: Record<string, Record<string, string>> = {
    default: {
      required: 'Input field must not be empty',
      email: 'The input is not valid E-mail!',
    },
  };

  dataForm = [
    {
      name: 'username',
      title: 'Username',
      type: 'text',
      formatter: () => 0,
      parser: () => '',
    },
    {
      name: 'firstName',
      title: 'First Name',
      type: 'text',
      formatter: () => 0,
      parser: () => '',
    },
    {
      name: 'lastName',
      title: 'Last Name',
      type: 'text',
      formatter: () => 0,
      parser: () => '',
    },
    {
      name: 'email',
      title: 'Email',
      type: 'email',
      formatter: () => 0,
      parser: () => '',
    },
    {
      name: 'birthDate',
      title: 'Birth Date',
      type: 'date',
      disableDate: (current: Date): boolean => differenceInCalendarDays(current, new Date()) >= 0,
      formatter: () => 0,
      parser: () => '',
    },
    {
      name: 'basicSalary',
      title: 'Basic Salary',
      type: 'number',
      formatter: (value: number | string) => `Rp${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ','),
      parser: (value: number | string) => `${value}`.replace(/\Rp\s?|(,*)/g, ''),
    },
    {
      name: 'group',
      title: 'Group',
      type: 'select',
      options: EMPLOYEE_GROUP_MAPPER,
      formatter: () => 0,
      parser: () => '',
    },

    {
      name: 'description',
      title: 'Description',
      type: 'textarea',
      formatter: () => 0,
      parser: () => '',
    },
    {
      name: 'status',
      title: 'Status',
      type: 'select',
      options: EMPLOYEE_STATUS_MAPPER,
      formatter: () => 0,
      parser: () => '',
    },
  ];

  async submitForm(): Promise<void> {
    if (this.validateForm.valid) {
      const data = this.validateForm.value as EmployeeType;
      const isCreated = await this.employeeService.create(data);
      if (isCreated) {
        this.message.success('Employee created!', { nzDuration: 3000 });
        await this.router.navigateByUrl(`/employee`);
      } else {
        this.message.error('Something is Error!', { nzDuration: 1500 });
      }
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.email, Validators.required]],
      birthDate: ['', [Validators.required]],
      basicSalary: ['', [Validators.required]],
      group: ['', [Validators.required]],
      description: ['', [Validators.required]],
      status: ['', [Validators.required]],
    });
  }
}
