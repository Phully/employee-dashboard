import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, Validators, UntypedFormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { NzMessageService } from 'ng-zorro-antd/message';

import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent implements OnInit {
  constructor(
    private loginService: LoginService,
    private message: NzMessageService,
    private fb: UntypedFormBuilder,
    private router: Router
  ) {}

  validateForm!: UntypedFormGroup;

  validateFormStatus: boolean = false;

  autoTips: Record<string, Record<string, string>> = {
    default: { required: 'Input field must not be empty' },
  };

  async submitForm(): Promise<void> {
    if (this.validateForm.valid) {
      this.validateFormStatus = true;

      const data = this.validateForm.value as Record<string, string>;
      const isLogin = await this.loginService.login(data['username'], data['password']);

      this.validateFormStatus = false;

      if (isLogin) {
        this.message.success('Login Successful!', { nzDuration: 3000 });
        await this.router.navigateByUrl(`/employee`);
      } else {
        this.message.error('Please check your username and password!', { nzDuration: 1500 });
      }
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
      remember: ['', []],
    });
  }
}
