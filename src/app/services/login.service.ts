import { Injectable } from '@angular/core';

import { sleep } from 'radash';
import { ImmortalDB } from 'immortal-db';

@Injectable()
export class LoginService {
  async login(username: string, password: string): Promise<boolean> {
    let result = false;
    if (username === 'admin' && password === 'admin') {
      await ImmortalDB.set('credentialToken', 'credential-token');
      result = true;
    }
    await sleep(1000);
    return result;
  }

  async logout(): Promise<boolean> {
    await sleep(1000);
    await ImmortalDB.remove('credentialToken');
    return true;
  }
}
