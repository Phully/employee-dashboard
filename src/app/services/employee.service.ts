import { Injectable } from '@angular/core';

import { random, sleep } from 'radash';
import { faker } from '@faker-js/faker';
import { ImmortalDB } from 'immortal-db';

import Fuse from 'fuse.js';

import { EMPLOYEE_GROUP_MAPPER, EMPLOYEE_STATUS_MAPPER } from '../common/mapper/employee.mapper';

@Injectable()
export class EmployeeService {
  dataSource = Array(2000)
    .fill(null)
    .map(() => ({
      username: faker.internet.userName(),
      firstName: faker.name.firstName(),
      lastName: faker.name.lastName(),
      email: faker.internet.email(),
      birthDate: faker.date.birthdate(),
      basicSalary: faker.finance.amount(1000000, 100000000),
      group: EMPLOYEE_GROUP_MAPPER[random(0, 9)],
      status: EMPLOYEE_STATUS_MAPPER[random(0, 1)],
      description: faker.lorem.words(50),
    }));

  fuseOptions = {
    keys: ['username', 'firstName', 'lastName', 'email', 'group', 'status'],
  };

  async findAll(): Promise<Array<EmployeeType>> {
    await sleep(1000);

    const dataSourceDb = await ImmortalDB.get('dataSource');

    if (dataSourceDb === null) {
      await ImmortalDB.set('dataSource', JSON.stringify(this.dataSource));
      return this.dataSource;
    } else {
      return JSON.parse(dataSourceDb);
    }
  }

  async findOne(searchKey: string): Promise<Array<EmployeeType>> {
    await sleep(1000);

    const dataSourceDb = await ImmortalDB.get('dataSource');

    if (dataSourceDb === null) {
      await ImmortalDB.set('dataSource', JSON.stringify(this.dataSource));
      return this.dataSource;
    } else {
      const dataSourceDbParse = JSON.parse(dataSourceDb);
      if (searchKey !== '') {
        const fuse = new Fuse<EmployeeType>(dataSourceDbParse, this.fuseOptions);
        const fuseData = fuse.search(searchKey);
        return fuseData.map(employeeData => employeeData.item);
      }
      return dataSourceDbParse;
    }
  }

  async create(employee: EmployeeType): Promise<boolean> {
    await sleep(1000);

    const dataSourceDb = await ImmortalDB.get('dataSource');

    if (dataSourceDb === null) {
      const newDateSource = [...this.dataSource, employee];
      await ImmortalDB.set('dataSource', JSON.stringify(newDateSource));
      return true;
    } else {
      const dataSourceDbParse = JSON.parse(dataSourceDb);
      await ImmortalDB.set('dataSource', JSON.stringify([...dataSourceDbParse, employee]));
      return dataSourceDbParse;
    }
  }
}
