import { Component } from '@angular/core';

import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

import { LoginService } from '../../services/login.service';

@Component({
  selector: 'app-dashboard-layout',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
})
export class DashboardComponent {
  constructor(private loginService: LoginService, private message: NzMessageService, private router: Router) {}

  logoutStatus: boolean = false;

  async logout(): Promise<void> {
    this.logoutStatus = true;

    const isLogout = await this.loginService.logout();

    this.logoutStatus = false;

    if (isLogout) {
      this.message.success('Logout Successful!', { nzDuration: 3000 });
      await this.router.navigateByUrl(`/`);
    } else {
      this.message.error('Something is Error!', { nzDuration: 1500 });
    }
  }
}
