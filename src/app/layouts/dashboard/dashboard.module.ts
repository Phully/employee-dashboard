import { NgModule } from '@angular/core';

import { RouterOutlet } from '@angular/router';

import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzButtonModule } from 'ng-zorro-antd/button';

import { LoginService } from '../../services/login.service';
import { DashboardComponent } from './dashboard.component';

@NgModule({
  imports: [RouterOutlet, NzLayoutModule, NzMenuModule, NzButtonModule],
  declarations: [DashboardComponent],
  providers: [LoginService],
  exports: [DashboardComponent],
})
export class DashboardModuleModule {}
