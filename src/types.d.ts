interface EmployeeType {
  username: string;
  firstName: string;
  lastName: string;
  email: string;
  birthDate: Date;
  basicSalary: string;
  group: string;
  description: string;
  status: string;
}
