# Update to nx, angular version 17, supabase (Done)
https://gitlab.com/Phully/employee-dashboard-v2

# Demo
https://employee-dashboard-xi.vercel.app/login

# Screenshoot (/screenshot for further information)

Angular v17
![Welcome](screenshot/auth.png "Welcome")

![Welcome](screenshot/dashboard.png "Welcome")

Angular Old
![Welcome](screenshot/1.png "Welcome")

![Welcome](screenshot/2.png "Welcome")

![Welcome](screenshot/3.png "Welcome")


